﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************
using System;
using System.Threading;
using System.Runtime.CompilerServices;

namespace Pk.Accessories.Threading.Synchronization
{
    public struct InterlockedState<T>
        where T : Enum
    {
        private int _State;

        public InterlockedState(T initialState)
        {
            this._State = Unsafe.As<T, int>(ref initialState);
        }

        public bool TrySetState(T newState, T expectedState)
        {
            int expectedStateInt = Unsafe.As<T, int>(ref expectedState);
            return expectedStateInt
                    == Interlocked.CompareExchange(ref this._State, Unsafe.As<T, int>(ref newState), expectedStateInt);
        }

        public void SetState(T newState, T expectedState)
        {
            int expectedStateInt = Unsafe.As<T, int>(ref expectedState);
            int oldStateInt = Interlocked.CompareExchange(ref this._State, Unsafe.As<T, int>(ref newState), expectedStateInt);
            if (expectedStateInt != oldStateInt)
            {
                throw new InvalidOperationException(
                    String.Format($"Can't set state {newState}. Current state is {Unsafe.As<int, T>(ref oldStateInt)}, but expected state was {expectedState}")
                    );
            }
        }

        public void SetState(T newState)
        {
            Interlocked.Exchange(ref this._State, Unsafe.As<T, int>(ref newState));
        }

        public T State
        {
            get
            {
                int state = Volatile.Read(ref this._State);
                return Unsafe.As<int, T>(ref state);
            }
        }

        static InterlockedState()
        {
            if (Enum.GetUnderlyingType(typeof(T)) != typeof(int))
            {
                throw new InvalidOperationException("Only underlying type int is supported");
            }
        }

    }
}
