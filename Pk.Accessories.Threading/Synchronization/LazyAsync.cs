﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************
using System;
using System.Threading.Tasks;
using System.Threading;
using Pk.Accessories.Threading.Abstractions;

namespace Pk.Accessories.Threading.Synchronization
{
    public class LazyAsync<T>: ILazyAsync<T>
    {
        private readonly Func<Task<T>> _Factory;
        private SpinLock _Lock;
        private Task<T> _Task;

        public LazyAsync(Func<Task<T>> factory)
        {
            this._Task = null;
            this._Factory = factory ?? throw new ArgumentNullException(nameof(factory));
            this._Lock = new SpinLock();
        }

        public LazyAsync(Task<T> value)
        {
            this._Task = value ?? throw new ArgumentNullException(nameof(value));
            this._Factory = null; //Will not be used
            this._Lock = default; //Will not be used
        }

        public Task<T> GetValueAsync()
        {
            if (this._Task == null)
            {
                StartInitialization();
            }
            return this._Task;
        }

        private void StartInitialization()
        {
            bool lockTaken = false;
            this._Lock.Enter(ref lockTaken);
            if (!lockTaken)
            {
                this._Task = Task.FromException<T>(new Exception("Unable to take a lock"));
                return;
            }
            try
            {
                if (this._Task == null)
                {
                    this._Task = this._Factory();
                }
            }
            catch(Exception ex)
            {
                this._Task = Task.FromException<T>(ex);
            }
            finally
            {
                this._Lock.Exit();
            }
        }
    }
}
