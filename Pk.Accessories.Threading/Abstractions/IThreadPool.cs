﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************

using System;
using System.Threading;
using System.Runtime.InteropServices;

namespace Pk.Accessories.Threading.Abstractions
{
    public interface IThreadPool
    {
        //long CompletedWorkItemCount { get; }

        //long PendingWorkItemCount { get; }

        //int ThreadCount { get; }

        void GetAvailableThreads(out int workerThreads, out int completionPortThreads);

        void GetMaxThreads(out int workerThreads, out int completionPortThreads);

        void GetMinThreads(out int workerThreads, out int completionPortThreads);

        bool SetMaxThreads(int workerThreads, int completionPortThreads);

        bool SetMinThreads(int workerThreads, int completionPortThreads);

        bool QueueUserWorkItem(WaitCallback callBack);

        bool QueueUserWorkItem(WaitCallback callBack, object state);

        RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state,
                                        uint millisecondsTimeOutInterval, bool executeOnlyOnce);

        RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state,
                                        TimeSpan timeout, bool executeOnlyOnce);

        RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state,
                                        int millisecondsTimeOutInterval, bool executeOnlyOnce);

        RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state,
                                        long millisecondsTimeOutInterval, bool executeOnlyOnce);

        bool UnsafeQueueUserWorkItem(WaitCallback callBack, object state);
        RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
            object state, TimeSpan timeout, bool executeOnlyOnce);

        RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
            object state, int millisecondsTimeOutInterval, bool executeOnlyOnce);

        RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
            object state, long millisecondsTimeOutInterval, bool executeOnlyOnce);

        RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
            object state, uint millisecondsTimeOutInterval, bool executeOnlyOnce);

        bool BindHandle(SafeHandle osHandle);

        unsafe bool UnsafeQueueNativeOverlapped(NativeOverlapped* overlapped);
    }
}
