﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************
using System;
using Pk.Accessories.Threading.Abstractions;
using NativePool = System.Threading.ThreadPool;
using System.Threading;
using System.Runtime.InteropServices;

namespace Pk.Accessories.Threading.ThreadPool
{
    public class ThreadPoolImpl: IThreadPool
    {
        protected ThreadPoolImpl()
        {
        }

        public static ThreadPoolImpl Instance { get; } = new ThreadPoolImpl();

        public void GetAvailableThreads(out int workerThreads, out int completionPortThreads)
            => NativePool.GetAvailableThreads(out workerThreads, out completionPortThreads);

        public void GetMaxThreads(out int workerThreads, out int completionPortThreads)
            => NativePool.GetMaxThreads(out workerThreads, out completionPortThreads);

        public void GetMinThreads(out int workerThreads, out int completionPortThreads)
            => NativePool.GetMinThreads(out workerThreads, out completionPortThreads);

        public bool SetMaxThreads(int workerThreads, int completionPortThreads)
            => NativePool.SetMaxThreads(workerThreads, completionPortThreads);

        public bool SetMinThreads(int workerThreads, int completionPortThreads)
            => NativePool.SetMinThreads(workerThreads, completionPortThreads);

        public bool QueueUserWorkItem(WaitCallback callBack)
            => NativePool.QueueUserWorkItem(callBack);

        public bool QueueUserWorkItem(WaitCallback callBack, object state)
            => NativePool.QueueUserWorkItem(callBack, state);

        public RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
                            object state, uint millisecondsTimeOutInterval, bool executeOnlyOnce)
            => NativePool.RegisterWaitForSingleObject(waitObject, callBack, state, millisecondsTimeOutInterval, executeOnlyOnce);

        public RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
                                        object state, TimeSpan timeout, bool executeOnlyOnce)
             => NativePool.RegisterWaitForSingleObject(waitObject, callBack, state, timeout, executeOnlyOnce);

        public RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
                                        object state, int millisecondsTimeOutInterval, bool executeOnlyOnce)
             => NativePool.RegisterWaitForSingleObject(waitObject, callBack, state, millisecondsTimeOutInterval, executeOnlyOnce);

        public RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
                                        object state, long millisecondsTimeOutInterval, bool executeOnlyOnce)
             => NativePool.RegisterWaitForSingleObject(waitObject, callBack, state, millisecondsTimeOutInterval, executeOnlyOnce);

        public bool UnsafeQueueUserWorkItem(WaitCallback callBack, object state)
            => NativePool.UnsafeQueueUserWorkItem(callBack, state);

        public RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
            object state, TimeSpan timeout, bool executeOnlyOnce)
        {
             return NativePool.UnsafeRegisterWaitForSingleObject(waitObject, callBack, state,
                                        timeout, executeOnlyOnce);
        }

        public RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
            object state, int millisecondsTimeOutInterval, bool executeOnlyOnce)
        {
            return NativePool.UnsafeRegisterWaitForSingleObject(waitObject, callBack, state,
                                    millisecondsTimeOutInterval, executeOnlyOnce);
        }
         
        public RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
            object state, long millisecondsTimeOutInterval, bool executeOnlyOnce)
        {
             return NativePool.UnsafeRegisterWaitForSingleObject(waitObject, callBack, state,
                                    millisecondsTimeOutInterval, executeOnlyOnce);
        }

        public RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack,
            object state, uint millisecondsTimeOutInterval, bool executeOnlyOnce)
        {
            return NativePool.UnsafeRegisterWaitForSingleObject(waitObject, callBack, state,
                                millisecondsTimeOutInterval, executeOnlyOnce);
        }

        public bool BindHandle(SafeHandle osHandle)
            => NativePool.BindHandle(osHandle);

        public unsafe bool UnsafeQueueNativeOverlapped(NativeOverlapped* overlapped)
            => NativePool.UnsafeQueueNativeOverlapped(overlapped);

    }
}
