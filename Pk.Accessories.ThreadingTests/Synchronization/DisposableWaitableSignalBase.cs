﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************

using NUnit.Framework;
using Pk.Accessories.Threading.Abstractions;
using System.Threading;
using Pk.NUnitExtension.Attributes;
using System;

namespace Pk.Accessories.ThreadingTests.Synchronization
{
    public abstract class DisposableWaitableSignalBase<T>
        where T: class, IDisposableWaitableSignal
    {
        #region Base props and TrearDown
        protected T _Tst;
        protected abstract T Create(bool initialState);
        protected abstract bool NativeWait(int timeout);

        private IDisposableWaitableSignal TestInterface => this._Tst;

        [TearDown()]
        public void TearDown()
        {
            if (this._Tst != null)
            {
                this._Tst.Dispose();
                this._Tst = null;
            }
        }
        #endregion Base props and TrearDown

        #region Tests
        [Test()]
        public void Signal()
        {
            this._Tst = Create(false);

            this.TestInterface.Signal();
            bool res = this.NativeWait(1);

            Assert.IsTrue(res);
        }

        [Test()]
        [Timeout(500)]
        public void Wait()
        {
            this._Tst = Create(true);

            this.TestInterface.Wait();

            Assert.Pass(); //Timeout wasn't reached
        }

        [DecoratedTestCase(true, TestName = "Signaled")]
        [DecoratedTestCase(false, TestName = "Not Signaled")]
        [Timeout(500)]
        public void WaitTimeout(bool signaled)
        {
            bool expRes = signaled;
            this._Tst = Create(signaled);

            bool res = this.NativeWait(1);

            Assert.AreEqual(expRes, res);
        }

        [Test()]
        public void Dispose()
        {
            this._Tst = Create(false);

            this.TestInterface.Dispose();

            Assert.Throws<ObjectDisposedException>(() => this.NativeWait(1));
        }
        #endregion Tests
    }
}
