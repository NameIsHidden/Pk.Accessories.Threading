﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************
using NUnit.Framework;
using System;
using Pk.Accessories.Threading.Synchronization;
using Pk.NUnitExtension.Attributes;

namespace Pk.Accessories.ThreadingTests.Synchronization
{
    public class InterlockedStateTests
    {
        [Test()]
        public void CtorDefault()
        {
            var o = new InterlockedState<TestEnum>();

            Assert.AreEqual(o.State, TestEnum.Default);
        }

        [DecoratedTestCase(TestEnum.Default, TestName = "Ctor(default)")]
        [DecoratedTestCase(TestEnum.State1, TestName = "Ctor(State1)")]
        [DecoratedTestCase(TestEnum.State2, TestName = "Ctor(State2)")]
        public void CtorInitialState(TestEnum state)
        {
            var o = new InterlockedState<TestEnum>(state);

            Assert.AreEqual(o.State, state);
        }

        [DecoratedTestCase(TestEnum.Default, TestEnum.Default, TestName = "Set same state")]
        [DecoratedTestCase(TestEnum.State2, TestEnum.State1, TestName = "Set different state")]
        public void TrySetState(TestEnum initialState, TestEnum newState)
        {
            var tst = new InterlockedState<TestEnum>(initialState);

            bool res = tst.TrySetState(newState, initialState);

            Assert.IsTrue(res);
            Assert.AreEqual(tst.State, newState);
        }

        [DecoratedTestCase(TestEnum.Default, TestEnum.Default, TestName = "Set same state")]
        [DecoratedTestCase(TestEnum.State2, TestEnum.State1, TestName = "Set different state")]
        public void SetState(TestEnum initialState, TestEnum newState)
        {
            var tst = new InterlockedState<TestEnum>(initialState);

            tst.SetState(newState, initialState);

            Assert.AreEqual(tst.State, newState);
        }

        [Test()]
        public void SetStateUnconditional()
        {
            var tst = new InterlockedState<TestEnum>(TestEnum.State1);

            tst.SetState(TestEnum.State2);

            Assert.AreEqual(tst.State, TestEnum.State2);
        }

        [DecoratedTestCase(TestEnum.Default, TestEnum.State1, TestEnum.State1, TestName = "Initial state=Default")]
        [DecoratedTestCase(TestEnum.State2, TestEnum.State1, TestEnum.Default, TestName = "Initial state<>Default")]
        public void TrySetStateUnexpected(TestEnum initialState, TestEnum newState, TestEnum expState)
        {
            var tst = new InterlockedState<TestEnum>(initialState);

            bool res = tst.TrySetState(newState, expState);

            Assert.IsFalse(res);
            Assert.AreEqual(tst.State, initialState);
        }

        [DecoratedTestCase(TestEnum.Default, TestEnum.State1, TestEnum.State1, TestName = "Initial state=Default")]
        [DecoratedTestCase(TestEnum.State2, TestEnum.State1, TestEnum.Default, TestName = "Initial state<>Default")]
        public void SetStateUnexpected(TestEnum initialState, TestEnum newState, TestEnum expState)
        {
            var tst = new InterlockedState<TestEnum>(initialState);

            Assert.Catch<InvalidOperationException>(() => tst.SetState(newState, expState));
            Assert.AreEqual(tst.State, initialState);
        }

        public void CtorEnumByteUnderlyingType()
        {
            Assert.Catch<InvalidOperationException>(() => _ = new InterlockedState<ByteEnum>());
        }

        public void CtorEnumByteLongType()
        {
            Assert.Catch<InvalidOperationException>(() => _ = new InterlockedState<LongEnum>());
        }

        enum ByteEnum: byte
        {
        }

        enum LongEnum: long
        {
        }

        public enum TestEnum
        {
            Default,
            State1,
            State2
        }
    }
}
