﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************
using NUnit.Framework;
using System;
using Pk.Accessories.Threading.Synchronization;

namespace Pk.Accessories.ThreadingTests.Synchronization
{
    [TestFixture()]
    public class SyncObjectFactoryTests
    {
        [Test()]
        public void InstanceIsTheSame()
        {
            var tst = SyncObjectFactory.Instance;

            Assert.AreSame(tst, SyncObjectFactory.Instance);
        }

        [Test()]
        public void CreateAutoResetEventSignaled()
        {
            using (var tst = SyncObjectFactory.Instance.CreateAutoResetEvent(true))
            {
                bool res1 = tst.Wait(1);
                bool res2 = tst.Wait(2);

                Assert.IsTrue(tst is EventWaitableSignal);
                Assert.IsTrue(res1);
                Assert.IsFalse(res2);
            }  
        }

        [Test()]
        public void CreateAutoResetEventNotSignaled()
        {
            using (var tst = SyncObjectFactory.Instance.CreateAutoResetEvent(true))
            {
                bool res1 = tst.Wait(1);

                Assert.IsTrue(tst is EventWaitableSignal);
                Assert.IsTrue(res1);
            }
        }

        [Test()]
        public void CreateManualResetEventSignaled()
        {
            using (var tst = SyncObjectFactory.Instance.CreateManualResetEvent(true))
            {
                bool res1 = tst.Wait(1);
                bool res2 = tst.Wait(2);

                Assert.IsTrue(tst is EventWaitableSignal);
                Assert.IsTrue(res1);
                Assert.IsTrue(res2);
            }   
        }

        [Test()]
        public void CreateManualResetEventNotSignaled()
        {
            using (var tst = SyncObjectFactory.Instance.CreateManualResetEvent(true))
            {
                bool res1 = tst.Wait(1);

                Assert.IsTrue(tst is EventWaitableSignal);
                Assert.IsTrue(res1);
            }
        }

        [Test()]
        public void CreateCountdownEvent()
        {
            using (var tst = SyncObjectFactory.Instance.CreateCountdownEvent(2))
            {
                Assert.IsTrue(tst is CountdownWaitableSignal);
                Assert.AreEqual(2, tst.InitialCount);
            } 
        }
    }
}
