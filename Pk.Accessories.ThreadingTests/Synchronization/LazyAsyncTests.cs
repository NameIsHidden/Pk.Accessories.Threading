﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************
using NUnit.Framework;
using System;
using Pk.Accessories.Threading.Synchronization;
using Pk.NUnitExtension.Attributes;
using System.Threading.Tasks;
using System.Linq;

namespace Pk.Accessories.ThreadingTests.Synchronization
{
    [Parallelizable(ParallelScope.All)]
    public class LazyAsyncTests
    {
        [Test()]
        public void CtorFactoryNullArgument()
        {
            Assert.Throws<ArgumentNullException>(() => _ = new LazyAsync<int>((Func<Task<int>>)null));
        }

        [Test()]
        public void CtorTaskNullArgument()
        {
            Assert.Throws<ArgumentNullException>(() => _ = new LazyAsync<int>((Task<int>)null));
        }

        [Test()]
        public async Task GetValueAsyncSynchronious()
        {
            var tst = new LazyAsync<int>(() =>
            {
                return Task<int>.FromResult(1);
            });

            var res = await tst.GetValueAsync();

            Assert.AreEqual(1, res);
        }

        [Test()]
        public async Task GetValueAsyncFromTask()
        {
            var tst = new LazyAsync<int>(Task<int>.FromResult(10));

            var res = await tst.GetValueAsync();

            Assert.AreEqual(10, res);
        }

        [Test()]
        public async Task GetValueAsync()
        {
            var tst = new LazyAsync<int>(async () =>
            {
                await Task.Yield();
                return 5;
            });

            var res = await tst.GetValueAsync();

            Assert.AreEqual(5, res);
        }

        [Test()]
        public void GetValueAsyncAsynchroniousException()
        {
            var tst = new LazyAsync<int>(async () =>
            {
                await Task.Yield();
                throw new TestException();
            });

            Assert.ThrowsAsync<TestException>(async () => _ = await tst.GetValueAsync());
        }

        [Test()]
        public void GetValueAsyncSynchroniousException()
        {
            var tst = new LazyAsync<int>(() =>
            {
                throw new TestException();
            });

            Assert.ThrowsAsync<TestException>(async () => _ = await tst.GetValueAsync());
        }

        [Test()]
        public async Task GetValueAsyncConcurent()
        {
            const int expected = 12345;
            var tst = new LazyAsync<TestObject>(async () =>
            {
                await Task.Yield();
                return new TestObject() { Value = expected };
            });
            TestObject res1 = new TestObject() { Value = 1 };
            TestObject res2 = new TestObject() { Value = 2 };
            TestObject res3 = new TestObject() { Value = 3 };

            await WhenAll(
                async () => { res1 = await tst.GetValueAsync(); },
                async () => { res2 = await tst.GetValueAsync(); },
                async () => { res3 = await tst.GetValueAsync(); }
            );

            Assert.AreEqual(expected, res1.Value);
            Assert.AreEqual(expected, res2.Value);
            Assert.AreEqual(expected, res3.Value);
        }

        [Test()]
        //[Ignore("Timeconsuming and unsafe. Manual run only")]
        public async Task GetValueAsyncDelayed()
        {
            const int delay = 2000;
            const int expected = 12345;
            var tst = new LazyAsync<int>(async () =>
            {
                await Task.Delay(delay);
                return expected;
            });
            int res1 = 0;
            int res2 = 1;
            int res3 = 2;

            await WhenAll(
                async () => { res1 = await tst.GetValueAsync(); },
                async () => { res2 = await tst.GetValueAsync(); },
                async () => { res3 = await tst.GetValueAsync(); }
            );

            Assert.AreEqual(expected, res1);
            Assert.AreEqual(expected, res2);
            Assert.AreEqual(expected, res3);
        }

        [Test()]
        //[Ignore("Timeconsuming and unsafe. Manual run only")]
        public async Task GetValueAsyncVariousDelays()
        {
            const int expected = 12345;
            var tst = new LazyAsync<int>(async () =>
            {
                await Task.Delay(2000);
                return expected;
            });
            int res1 = 0;
            int res2 = 1;
            int res3 = 2;

            await WhenAll(
                async () => { res1 = await tst.GetValueAsync(); },
                async () => { res2 = await GetValueWithDelayAsync(tst, 1000); },
                async () => { res3 = await GetValueWithDelayAsync(tst, 3000); }
            );

            Assert.AreEqual(expected, res1);
            Assert.AreEqual(expected, res2);
            Assert.AreEqual(expected, res3);
        }

        private static async Task<T> GetValueWithDelayAsync<T>(LazyAsync<T> lazy, int delay)
        {
            await Task.Delay(delay);
            return await lazy.GetValueAsync();
        }

        private static Task WhenAll(params Func<Task>[] args)
        {
            var tasks = args.Select(t => t()).ToArray();
            return Task.WhenAll(tasks);
        }

        private class TestException: Exception
        {

        }

        private class TestObject
        {
            public int Value { get; set; }
        }
    }
}
