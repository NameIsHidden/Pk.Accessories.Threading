﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************

using NUnit.Framework;
using Pk.Accessories.Threading.Abstractions;
using Pk.Accessories.Threading.Synchronization;
using System.Threading;

namespace Pk.Accessories.ThreadingTests.Synchronization
{
    [TestFixture()]
    //Since class derived from standard framework's implementation, the intent of tests is just to verify
    //that ctor and interface metods properly pass parameters and calls to appropriate .net methods.
    //Testing framework functionality is out of the scope.
    public class CountdownWaitableSignalTests : DisposableWaitableSignalBase<CountdownWaitableSignal>
    {
        #region Base methods and props
        protected override CountdownWaitableSignal Create(bool initialState)
            => new CountdownWaitableSignal(initialState ? 0 : 1);

        protected override bool NativeWait(int timeout)
            => Native.Wait(timeout);

        private ICountdownEvent TestInterface => this._Tst;

        private CountdownEvent Native => this._Tst;
        #endregion Base methods and props

        #region Tests
        [Test()]
        public void Ctor()
        {
            this._Tst = new CountdownWaitableSignal(3);

            Assert.AreEqual(3, this._Tst.InitialCount);
            Assert.AreEqual(3, this._Tst.CurrentCount);
        }

        [Test()]
        public void CurrentCount()
        {
            this._Tst = new CountdownWaitableSignal(5);

            this.Native.Signal();

            Assert.AreEqual(5, this._Tst.InitialCount);
            Assert.AreEqual(4, this._Tst.CurrentCount);
        }

        [Test()]
        public void Reset()
        {
            this._Tst = new CountdownWaitableSignal(5);
            this.NativeWait(1);

            this.TestInterface.Reset();

            Assert.AreEqual(5, this._Tst.InitialCount);
            Assert.AreEqual(5, this._Tst.CurrentCount);
        }

        [Test()]
        public void ResetCount()
        {
            this._Tst = new CountdownWaitableSignal(5);

            this.TestInterface.Reset(12);

            Assert.AreEqual(12, this._Tst.InitialCount);
            Assert.AreEqual(12, this._Tst.CurrentCount);
        }

        [Test()]
        public void AddCount()
        {
            this._Tst = new CountdownWaitableSignal(5);

            this.TestInterface.AddCount();

            Assert.AreEqual(5, this._Tst.InitialCount);
            Assert.AreEqual(6, this._Tst.CurrentCount);
        }

        [Test()]
        public void TryAddCount()
        {
            this._Tst = new CountdownWaitableSignal(5);

            bool res = this.TestInterface.TryAddCount();

            Assert.IsTrue(res);
            Assert.AreEqual(5, this._Tst.InitialCount);
            Assert.AreEqual(6, this._Tst.CurrentCount);
        }
        #endregion Tests
    }
}
