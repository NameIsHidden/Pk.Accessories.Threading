﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************

using NUnit.Framework;
using Pk.Accessories.Threading.Abstractions;
using Pk.Accessories.Threading.Synchronization;
using System.Threading;
using Pk.NUnitExtension.Attributes;

namespace Pk.Accessories.ThreadingTests.Synchronization
{
    [TestFixture()]
    //Since class derived from standard framework's implementation, the intent of tests is just to verify
    //that ctor and interface metods properly pass parameters and calls to appropriate .net methods.
    //Testing framework functionality is out of the scope.
    public class EventWaitableSignalTests: DisposableWaitableSignalBase<EventWaitableSignal>
    {
        #region Setup and Teardown
        protected override EventWaitableSignal Create(bool initialState)
            => new EventWaitableSignal(initialState, EventResetMode.ManualReset);

        protected override bool NativeWait(int timeout)
            => ((EventWaitHandle)this._Tst).WaitOne(timeout);
        #endregion Setup and Teardown

        #region Tests
        [DecoratedTestCase(true, EventResetMode.AutoReset, true, false, TestName = "AutoReset, Signaled")]
        [DecoratedTestCase(false, EventResetMode.AutoReset, false, false, TestName = "AutoReset, Not Signaled")]
        [DecoratedTestCase(true, EventResetMode.ManualReset, true, true, TestName = "ManualReset, Signaled")]
        [DecoratedTestCase(false, EventResetMode.ManualReset, false, false, TestName = "ManualReset, Not Signaled")]
        public void Ctor(bool initialState, EventResetMode mode, bool expRes1, bool expRes2)
        {
            this._Tst = new EventWaitableSignal(initialState, mode);

            bool res1 = this.NativeWait(1);
            bool res2 = this.NativeWait(1);

            Assert.AreEqual(expRes1, res1);
            Assert.AreEqual(expRes2, res2);
        }

        [DecoratedTestCase(true, TestName="Signaled")]
        [DecoratedTestCase(true, TestName = "Not Signaled")]
        public void Reset(bool signaled)
        {
            bool expRes = signaled;
            this._Tst = new EventWaitableSignal(signaled, EventResetMode.ManualReset);

            bool res = ((IEvent)this._Tst).Reset();
            bool stateIsSignaled = this.NativeWait(1);

            Assert.AreEqual(expRes, res);
            Assert.IsFalse(stateIsSignaled);
        }
        #endregion Tests
    }
}
