﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************

using NUnit.Framework;
using System;
using System.Threading;
using Pk.Accessories.Threading.ThreadPool;
using Pk.Accessories.Threading.Abstractions;
using OrigPool = System.Threading.ThreadPool;
using Pk.NUnitExtension.Attributes;
using NSubstitute;

namespace Pk.Accessories.ThreadingTests.ThreadPool
{
    //The metter of tests is only verify that implementation calls appropriate original methods.
    //[Retry] attribute used for methods that verify equality original ThreadPool function output and IPool method output.
    //Since some of the parameters are volatile and can be changed outside of test execution, need to give test a another chance if it fails
    //Some functions tested twice with different arguments to doublecheck that hardcoded arguments doesn't match initial framework settings
    [TestFixture()]
    public class ThreadPoolImplTests
    {
        #region Setup
        private IThreadPool _Pool;

        [SetUp()]
        public void Setup()
        {
            this._Pool = ThreadPoolImpl.Instance;
        }
        #endregion Setup

        #region IThreadPool tests
        [Test()]
        public void InstanceRefIsPermanent()
        {
            var secondRef = ThreadPoolImpl.Instance;

            Assert.AreSame(secondRef, this._Pool);
        }

        [Test()]
        [Retry(5)] //Thread number is volatile
        public void GetAvailableThreads()
        {
            OrigPool.GetAvailableThreads(out int expWorker, out int expCompletion);

            this._Pool.GetAvailableThreads(out int actWorker, out int actCompletion);

            Assert.AreEqual(expWorker, actWorker);
            Assert.AreEqual(expCompletion, actCompletion);
        }

        [Test()]
        [Retry(5)]
        public void GetMaxThreads()
        {
            OrigPool.GetMaxThreads(out int expWorker, out int expCompletion);

            this._Pool.GetMaxThreads(out int actWorker, out int actCompletion);

            Assert.AreEqual(expWorker, actWorker);
            Assert.AreEqual(expCompletion, actCompletion);
        }

        [Test()]
        [Retry(5)]
        public void GetMinThreads()
        {
            OrigPool.GetMinThreads(out int expWorker, out int expCompletion);

            this._Pool.GetMinThreads(out int actWorker, out int actCompletion);

            Assert.AreEqual(expWorker, actWorker);
            Assert.AreEqual(expCompletion, actCompletion);
        }

        [TestCase(6, 7)]
        [TestCase(7, 8)]
        public void SetMaxThreads(int worker, int completion)
        {
            bool res = this._Pool.SetMaxThreads(worker, completion);

            OrigPool.GetMaxThreads(out int actWorker, out int actCompletion);
            Assert.IsTrue(res);
            Assert.AreEqual(worker, actWorker);
            Assert.AreEqual(completion, actCompletion);
        }

        [TestCase(3, 4)]
        [TestCase(4, 5)]
        public void SetMinThreads(int worker, int completion)
        {
            bool res = this._Pool.SetMinThreads(worker, completion);

            OrigPool.GetMinThreads(out int actWorker, out int actCompletion);
            Assert.IsTrue(res);
            Assert.AreEqual(worker, actWorker);
            Assert.AreEqual(completion, actCompletion);
        }

        [Test()]
        public void QueueWorkItem()
        {
            using (var callback = new CallbackWrapper())
            {
                this._Pool.QueueUserWorkItem(callback.WaitCallback);
                callback.Wait();

                callback.WaitCallback.Received(1).Invoke(null);
            }
        }

        [Test()]
        public void QueueWorkItemArg()
        {
            using (var callback = new CallbackWrapper())
            {
                var o = new object();

                this._Pool.QueueUserWorkItem(callback.WaitCallback, o);
                callback.Wait();

                callback.WaitCallback.Received(1).Invoke(o);
            }
        }

        [Test()]
        public void UnsafeQueueWorkItem()
        {
            using (var callback = new CallbackWrapper())
            {
                var o = new object();

                this._Pool.UnsafeQueueUserWorkItem(callback.WaitCallback, o);
                callback.Wait();

                callback.WaitCallback.Received(1).Invoke(o);
            }
        }

        [TestRegisterWaitForSingleObject()]
        public void RegisterWaitForSingleObject(RegisterWaitForSingleObjectAction act)
        {
            using (var callback = new WaitOrTimerCallbackWrapper())
            using (var wait = new ManualResetEvent(true))
            {
                var o = new object();

                act(this._Pool, wait, callback.WaitOrTimerCallback, o, 1, true);
                callback.Wait();

                callback.WaitOrTimerCallback.Received(1).Invoke(o, false);
            }
        }

        [TestRegisterWaitForSingleObject()]
        public void RegisterWaitForSingleObjectTimeout(RegisterWaitForSingleObjectAction act)
        {
            var callback = new WaitOrTimerCallbackWrapper();
            var wait = new ManualResetEvent(false);
            var o = new object();

            act(this._Pool, wait, callback.WaitOrTimerCallback, o, 1, true);
            callback.Wait();

            callback.WaitOrTimerCallback.Received(1).Invoke(o, true);
        }
        #endregion IThreadPool tests

        #region Test Internals
        private class CallbackBase: IDisposable
        {
            protected EventWaitHandle _Wait { get; }

            protected CallbackBase()
            {
                this._Wait = new ManualResetEvent(false);
            }

            public void Wait()
            {
                bool res = this._Wait.WaitOne(500);
                if (!res)
                {
                    throw new AssertionException("Timeout");
                }
            }

            public void Dispose()
            {
                this._Wait.Dispose();
            }
        }

        private class CallbackWrapper: CallbackBase
        {
            public WaitCallback WaitCallback { get; }

            public CallbackWrapper()
            {
                this.WaitCallback = Substitute.For<WaitCallback>();
                this.WaitCallback.When(x => x.Invoke(Arg.Any<object>())).Do(x => this._Wait.Set());
            }
        }

        private class WaitOrTimerCallbackWrapper: CallbackBase
        {
            public WaitOrTimerCallback WaitOrTimerCallback { get; }

            public WaitOrTimerCallbackWrapper()
            {
                this.WaitOrTimerCallback = Substitute.For<WaitOrTimerCallback>();
                this.WaitOrTimerCallback.When(x => x.Invoke(Arg.Any<object>(), Arg.Any<bool>())).Do(x => this._Wait.Set());
            }
        }

        public delegate RegisteredWaitHandle RegisterWaitForSingleObjectAction(IThreadPool pool, WaitHandle waitObject, WaitOrTimerCallback callBack,
                            object state, uint millisecondsTimeOutInterval, bool executeOnlyOnce);

        public class TestRegisterWaitForSingleObjectAttribute : CompositeTestCaseAttribute
        {
            public TestRegisterWaitForSingleObjectAttribute()
                :base(Array.Empty<object>(),
                     Build<RegisterWaitForSingleObjectAction>(
                         ("int", (p, w, c, s, tm, ew) => p.RegisterWaitForSingleObject(w, c, s, tm, ew)),
                         ("uint", (p, w, c, s, tm, ew) => p.RegisterWaitForSingleObject(w, c, s, (uint)tm, ew)),
                         ("TimeSpan", (p, w, c, s, tm, ew) => p.RegisterWaitForSingleObject(w, c, s, TimeSpan.FromMilliseconds(tm), ew)),
                         ("long", (p, w, c, s, tm, ew) => p.RegisterWaitForSingleObject(w, c, s, (long)tm, ew)),

                         ("Unsafe,int", (p, w, c, s, tm, ew) => p.UnsafeRegisterWaitForSingleObject(w, c, s, tm, ew)),
                         ("Unsafe,uint", (p, w, c, s, tm, ew) => p.UnsafeRegisterWaitForSingleObject(w, c, s, (uint)tm, ew)),
                         ("Unsafe,TimeSpan", (p, w, c, s, tm, ew) => p.UnsafeRegisterWaitForSingleObject(w, c, s, TimeSpan.FromMilliseconds(tm), ew)),
                         ("Unsafe,long", (p, w, c, s, tm, ew) => p.UnsafeRegisterWaitForSingleObject(w, c, s, (long)tm, ew))
                     ))
            {
            } 
        }
        #endregion Test Internals
    }
}
